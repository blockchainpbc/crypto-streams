const ccxt = require("ccxt");

module.exports = async function fetchMarketIDFromSymbol (symbol, exch) {
  // let exch = new ccxt[exchange]();
  await exch.load_markets();
  if (!exch.markets[symbol]) {
    throw new Error('Symbol ' + symbol + ' not found ' + exch.name)
    return '';
  }
  return exch.markets[symbol].id;
};

